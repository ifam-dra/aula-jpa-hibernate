import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Estado;
import util.JPAUtil;

public class TestaEstado{

    public static void main(String[] args) {

        EntityManager em = JPAUtil.getEntityManager();

        Estado estado=new Estado();
        estado.setNome("Amazonas");
        estado.setSigla("AM");

        em.getTransaction().begin();
        em.persist(estado);
        em.getTransaction().commit();

        em.close();

    }

}
