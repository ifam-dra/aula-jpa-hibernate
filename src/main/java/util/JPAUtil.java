package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

    private static EntityManagerFactory fabrica =
            Persistence.createEntityManagerFactory("dra-aula-pu");

    public static EntityManager getEntityManager(){
        return fabrica.createEntityManager();
    }

}
