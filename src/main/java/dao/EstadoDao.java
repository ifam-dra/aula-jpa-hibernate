package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Estado;

public class EstadoDao {

    private EntityManager em;

    public EstadoDao(EntityManager em) {
        this.em = em;
    }

    public void salvar(Estado estado) {

        em.persist(estado);


    }

    public Estado consultar(long id) {

        return em.find(Estado.class,id);

    }

    public void excluir(Estado estado) {

        em.remove(estado);

    }

    public List<Estado> listar() {

        Query consulta = em.createQuery("select estado from Estado estado");

        return consulta.getResultList();

    }
}
