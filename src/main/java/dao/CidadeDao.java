package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Cidade;

public class CidadeDao {

    private EntityManager em;

    public CidadeDao(EntityManager em) {
        this.em = em;
    }

    public void salvar(Cidade cidade) {

        em.persist(cidade);


    }

    public Cidade consultar(long id) {

        return em.find(Cidade.class,id);

    }

    public void excluir(Cidade cidade) {

        em.remove(cidade);

    }

    public List<Cidade> listar() {

        Query consulta = em.createQuery("select cidade from Cidade cidade");

        return consulta.getResultList();

    }
}
