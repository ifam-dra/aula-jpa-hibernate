import java.util.List;

import javax.persistence.EntityManager;

import dao.CidadeDao;
import model.Cidade;
import util.JPAUtil;

public class TestaCidadeDao {

    public static void main(String[] args) {

        //Crio entity manager.
        EntityManager em = JPAUtil.getEntityManager();

        //Crio DAO
        CidadeDao cidadeDao=new CidadeDao(em);

        // Crio objeto Cidade
        Cidade cidade1=new Cidade();
        cidade1.setNome("Manaus");
        cidade1.setIbge("2222222");


        // Salvo objeto Cidade

        em.getTransaction().begin();

        cidadeDao.salvar(cidade1);

        em.getTransaction().commit();


        // Consulto objeto Cidade

        Cidade cidadeAchada = cidadeDao.consultar(1L);

        System.out.println(cidadeAchada);


        // Excluo objeto cidade que foi encontrado.


        em.getTransaction().begin();
        cidadeDao.excluir(cidadeAchada);
        em.getTransaction().commit();


        // Listo os objetos salvos no banco de dados.

        List<Cidade> cidades = cidadeDao.listar();

        for(Cidade c:cidades) {
            System.out.println(c);
        }

    }

}
