import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Cidade;
import model.Estado;
import util.JPAUtil;

public class TestaCidade {

    public static void main(String[] args) {

        EntityManager em= JPAUtil.getEntityManager();

        Cidade cidade = new Cidade();
        cidade.setNome("Itacoatiara");
        cidade.setIbge("122222");

        em.getTransaction().begin();
        em.persist(cidade);
        em.getTransaction().commit();


        Cidade cidadeAchada=em.find(Cidade.class,2L);

//        System.out.println("Cidade:"+cidadeAchada.getNome());

        em.getTransaction().begin();
        em.remove(cidadeAchada);
        em.getTransaction().commit();

//        Cidade cidadeAchada=em.find(Cidade.class,2L);
//
//        System.out.println("Cidade:"+cidadeAchada.getNome());

//        em.getTransaction().begin();

//        em.remove(cidadeAchada);

//        em.getTransaction().commit();

        //1.Criar a consulta
        Query consulta = em.createQuery("select c from Cidade c");
        //2.Executar a consulta
        List<Cidade> cidades = consulta.getResultList();


        for(Cidade c:cidades){
            System.out.println(c);
        }


        em.close();

    }

}
