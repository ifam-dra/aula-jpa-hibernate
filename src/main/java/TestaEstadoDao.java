import javax.persistence.EntityManager;

import dao.CidadeDao;
import dao.EstadoDao;
import model.Cidade;
import model.Estado;
import util.JPAUtil;

public class TestaEstadoDao {

    public static void main(String[] args) {

        Estado estado=new Estado();
        estado.setNome("Santa Catarina");
        estado.setSigla("SC");

        Cidade cidade=new Cidade();
        cidade.setNome("Florianopolis");
        cidade.setIbge("111111");

        cidade.setEstado(estado);


        EntityManager em = JPAUtil.getEntityManager();

        EstadoDao estadoDao=new EstadoDao(em);
        CidadeDao cidadeDao=new CidadeDao(em);

        em.getTransaction().begin();

        estadoDao.salvar(estado);
        cidadeDao.salvar(cidade);

        em.getTransaction().commit();

        em.close();

    }
}
